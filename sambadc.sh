#!/bin/bash
#
# sambdadc.sh
#
# Autor     : Caio Bentes <caio.bentes@solustecnologia.com.br>
#
#  -------------------------------------------------------------
#   
#

#Atualiza a lista de pacotes e atualiza pacotes
apt-get update && apt-get upgrade -y
#Instalo dependencias
apt-get install -y vim acl attr autoconf bind9utils bison build-essential \
debhelper dnsutils docbook-xml docbook-xsl flex gdb libjansson-dev krb5-user \
libacl1-dev libaio-dev libarchive-dev libattr1-dev libblkid-dev libbsd-dev \
libcap-dev libcups2-dev libgnutls28-dev libgpgme-dev libjson-perl \
libldap2-dev libncurses5-dev libpam0g-dev libparse-yapp-perl \
libpopt-dev libreadline-dev nettle-dev perl perl-modules pkg-config \
python-all-dev python-crypto python-dbg python-dev python-dnspython \
python3-dnspython python-gpgme python3-gpgme python-markdown python3-markdown \
python3-dev xsltproc zlib1g-dev liblmdb-dev lmdb-utils ntp  isc-dhcp-server && echo OK


#Seto as confirações do ip fixo do DC Samba
#read -p "Qual seu dominio? " MEUDOMINIO
read -p "Qual seu IP? " MEUIP
read -p "Qual sya máscara de rede IP? " MINHAMASCARADEREDE
read -p "Qual seu endereço de rede? " MEUENDERECODEREDE
read -p "Qual seu gateway? " MEUGATEWAY
read -p "Qual seu broadcast da rede? " MEUBROADCAST

export MEUDOMINIO
export MEUIP
export MINHAMASCARADEREDE
export MEUENDERECODEREDE
export MEUGATEWAY
export MEUBROADCAST

sed -i 's/^iface enp0s3/#iface enp0s3/' /etc/network/interfaces
echo "# Static IP address" >> /etc/network/interfaces
echo "iface enp0s3 inet static" >> /etc/network/interfaces
echo "address ${MEUIP}" >> /etc/network/interfaces
echo "netmask ${MINHAMASCARADEREDE}" >> /etc/network/interfaces
echo "network ${MEUENDERECODEREDE}" >> /etc/network/interfaces
echo "broadcast ${MEUBROADCAST}" >> /etc/network/interfaces
echo  "gateway ${MEUGATEWAY}" >> /etc/network/interfaces
/etc/init.d/networking restart
ifup enp0s3


cp -pRf /etc/network/interfaces /etc/network/interfaces.bkp
echo "# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

auto enp0s3
  iface enp0s3 inet static
    address 192.168.10.175
    netmask 255.255.255.0
    gateway 192.168.10.1
    domain-name-servers 8.8.8.8 8.8.4.4" > /etc/network/interfaces



echo '#Implementing Samba 4
ntpsigndsocket /usr/local/samba/var/lib/ntp_signd/
restrict default mssntp' >> /etc/ntp.conf


read -p "Qual seu dominio? " MEUDOMINIO
read -p "Qual seu IP? " MEUIP
read -p "Qual seu endereço de rede? " MEUENDERECODEREDE
read -p "Qual seu gateway? " MEUGATEWAY
read -p "Qual seu broadcast da rede? " MEUBROADCAST
echo "# Implementing Samba 4
ddns-updates on;
option domain-name "sindifisco.local";
option domain-name-servers ${MEUIP}, ${MEUIP};
option netbios-name-servers ${MEUIP};
option ntp-servers ${MEUIP};
default-lease-time 600;
max-lease-time 7200;
log-facility local7;
subnet ${MEUENDERECODEREDE} netmask ${MINHAMASCARADEREDE} {
range 192.168.10.250 192.168.10.253;
option routers ${MEUIP}; }" >> /etc/dhcp/dhcpd.conf


sed -i '17,18s/^/#/' /etc/default/isc-dhcp-server
echo INTERFACES="enp0s3" >> /etc/default/isc-dhcp-server

/etc/init.d/isc-dhcp-server start

#Seto hostname
hostnamectl set-hostname dc01
systemctl restart systemd-hostnamed
#Faço meu host resolver pro meu ip
echo "192.168.10.175 dc01.sindifisco.local dc01" >> /etc/hosts

unset MEUIP
unset MINHAMASCARADEREDE
unset MEUENDERECODEREDE
unset MEUGATEWAY
unset MEUBROADCAST

#Restarto o serviço de Rede
systemctl restart networking

#Muso o diretório para o /tmp
cd /tmp
#Faço download do samba em sua sersão 4.8.3
wget https://download.samba.org/pub/samba/stable/samba-4.8.0.tar.gz
#Descompacto os arquivos do samba
tar -zxvf samba-4.8.0.tar.gz
#Removo o Samba compactado
rm -f samba-4.8.0.tar.gz
#Entro na pasta do samba
cd samba-4.8.0
#Vamos gerar o Makefile e verificar se não há nenhuma dependência faltando e alguns ajustes extras
#./configure --enable-debug --enable-selftest
./configure --prefix /usr --enable-fhs --enable-cups --enable-debug --enable-selftest --sysconfdir=/etc --localstatedir=/var --with-privatedir=/var/lib/samba/private --with-piddir=/var/run/samba --with-automount --datadir=/usr/share --with-lockdir=/var/run/samba --with-statedir=/var/lib/samba --with-cachedir=/var/cache/samba --with-systemd
#Vamos compilar o Samba 4
make
#Vamos a instalação dos arquivos, comandos, e bibliotecas nos seus respectivos diretórios
make quicktest
make install
#Vamos atualizar o cache de bibliotecas dinâmicas 

#echo 'export PATH=/usr/local/samba/bin:/usr/local/samba/sbin:$PATH' >> /root/.bash_profile && echo OK
#samba-tool domain provision --realm=SINDIFISCO.LOCAL --domain=SINDIFISCO --adminpass='Solus@1010' --server-role='domain controller'
#samba-tool domain provision --server-role=dc --use-rfc2307 --dns-backend=SAMBA_INTERNAL --realm=SINDIFISCO.LOCAL --domain=SINDIFISCO --adminpass=Solus@1010


#cp -pRf /usr/local/samba/private/krb5.conf /etc/ && echo OK
samba-tool domain provision --use-rfc2307 --interactive

#Removendo a complexidade das senhas
samba-tool domain passwordsettings set --complexity=off

#Removendo tempo de expiração da senha do usuário administrator. Pode ser usado com outros usuários.
samba-tool user setexpiry administrator --noexpiry




#cp -pRf /usr/local/samba/etc/smb.conf /usr/local/samba/etc/smb.conf-bkp \
#&& sed -e 's/dns forwarder =.*$/dns forwarder = 8.8.8.8/g' /usr/local/samba/etc/smb.conf > /usr/local/samba/etc/smb.conf-new \
#&& mv /usr/local/samba/etc/smb.conf-new /usr/local/samba/etc/smb.conf && echo OK


#cp -pRf /usr/local/samba/etc/smb.conf \
#/usr/local/samba/etc/smb.conf-bkp && \
#sed -e 's/dns forwarder =.*$/dns forwarder = 8.8.8.8/g' /usr/local/samba/etc/smb.conf > /usr/local/samba/etc/smb.conf-new && \
#mv /usr/local/samba/etc/smb.conf-new /usr/local/samba/etc/smb.conf && \
#echo OK

#/usr/local/samba/sbin/samba -i -M single




install bind9 bind9-doc dnsutils -y
vim /etc/bind/named.conf.options







apt-get update
apt-get install make gcc
apt-get install linux-headers-$(uname -r)
init 6
#No Oracle VM VirtualBox, acesse o menu Dispositivo > Inserir imagem de CD dos Adicionais para Convidado
#Dar duplo clique no ícone VBOXADDTIONS_5.1.24_117012 (ou outra versão) que vai aparecer na área de trabalho
mount /dev/sr0 /media/cdrom0/
cd /media/cdrom0/
sudo sh ./VBoxLinuxAdditions*.run

cat PermitRootLogin yes >> /etc/ssh/sshd_config
/etc/init.d/ssh restart











#Seto hostname
hostnamectl set-hostname dc01
systemctl restart systemd-hostnamed
#Faço meu host resolver pro meu ip
echo "192.168.10.143 dc01.sindifisco.local dc01" >> /etc/hosts
#Instalar ferramentas de desenvolvimento
yum groupinstall 'Development Tools' -y
#Desabilita o firewall
systemctl stop firewalld
systemctl disable firewalld
#Desabilita o SELinux
sed -i '/SELINUX/ s/enforcing/disabled/g' /etc/selinux/config
setenforce 0
echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf

#Instala pacotes nescessários para compilação do samba
apt-get install -y perl gcc attr libacl-devel libblkid-devel gnutls-devel readline-devel \
python-devel gdb pkgconfig krb5-workstation zlib-devel setroubleshoot-server \
libaio-devel setroubleshoot-plugins policycoreutils-python libsemanage-python \
perl-ExtUtils-MakeMaker perl-Parse-Yapp perl-Test-Base popt-devel libxml2-devel \
libattr-devel keyutils-libs-devel cups-devel bind-utils libxslt docbook-style-xsl \
openldap-devel autoconf python-crypto pam-devel ntp wget vim

#Adiciono configurações samba no ntp 
cat <<EOF >> /etc/ntp.conf
# Configurações adicionais para o Samba 4
ntpsigndsocket /var/lib/samba/ntp_signd/
restrict default mssntp:
EOF
#Restarto o serviço do ntp
systemctl restart ntpd

#Muso o diretório para o /tmp
cd /tmp
#Faço download do samba em sua sersão 4.8.3
wget https://download.samba.org/pub/samba/stable/samba-4.8.2.tar.gz
#Descompacto os arquivos do samba
tar -zxvf samba-4.8.2.tar.gz
#Removo o Samba compactado
rm -f samba-4.8.2.tar.gz
#Entro na pasta do samba
cd samba-4.8.2
#Vamos gerar o Makefile e verificar se não há nenhuma dependência faltando e alguns ajustes extras
./configure --prefix /usr --enable-fhs --enable-cups --sysconfdir=/etc --localstatedir=/var --with-privatedir=/var/lib/samba/private --with-piddir=/var/run/samba --with-automount --datadir=/usr/share --with-lockdir=/var/run/samba --with-statedir=/var/lib/samba --with-cachedir=/var/cache/samba --with-systemd
#Vamos compilar o Samba 4
make
#Vamos a instalação dos arquivos, comandos, e bibliotecas nos seus respectivos diretórios
make install
#Vamos atualizar o cache de bibliotecas dinâmicas







 	
yum install bind bind-utils -y

ldconfig

cat <<EOF > /etc/krb5.conf
[libdefaults]
    dns_lookup_realm = false
    dns_lookup_kdc = true
    default_realm = ATIVALAB.BR
EOF


kinit ADMINISTRADOR

klist

samba-tool domain join --option="interfaces=lo enp0s3" --option="bind interfaces only=yes"

samba-tool domain join ativacob.br DC -U"ATIVACOB\administrador" --dns-backend=BIND9_DLZ --option="interfaces=lo enp0s3" --option="bind interfaces only=yes"
samba-tool domain join ativalab.br DC -U"ATIVALAB\administrador"

#samba-tool domain join ativacob.br DC -U"ATIVACOB\administrador" --dns-backend=SAMBA_INTERNAL


samba_dnsupdate --verbose --all-names
host -t SRV _ldap._tcp.Default-First-Site-Name._sites.gc._msdcs.ativalab.br\ATIVALAB.BR win-c4079ch0vjv
host -t CNAME 8a6bbd71-f44c-45ee-a9f3-a3b50a60ab89._msdcs.ativalab.br.
host -t A caio.ativalab.br.
host 192.168.10.128
samba_dnsupdate --verbose --all-names
samba
dig -t any ativalab.br
samba-tool dns query localhost ativalab.br @ ALL
samba-tool dns query localhost ativalab.br @ ALL 
samba-tool drs kcc -Uadministrador win-c4079ch0vjv.ativalab.br


cp -pRf /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf-bkp && sed -e 's/192.168.10.128/192.168.1.1/g' /etc/dhcp/dhcpd.conf > /etc/dhcp/dhcpd.conf-dnspatch && mv /etc/dhcp/dhcpd.conf-dnspatch /etc/dhcp/dhcpd.conf && echo OK

samba-tool dns delete 192.168.10.105 ativalab.br @ NS win-c4079ch0vjvativalab.br




sed -i '7i\dns forwarder = 8.8.8.8' /etc/samba/smb.conf
reboot





touch /lib/systemd/system/samba-ad-dc.service

cat <<EOF > /lib/systemd/system/samba-ad-dc.service
[Unit]
Description=Samba4 AD DC
After=network.target remote-fs.target nss-lookup.target 

[Service]
Type=forking
LimitNOFILE=16384
ExecStart=/usr/sbin/samba -D
ExecReload=/usr/bin/kill -HUP $MAINPID
PIDFile=/var/run/samba/samba.pid 

[Install]

WantedBy=multi-user.target:wq
EOF

systemctl daemon-reload 
systemctl start samba-ad-dc

#echo "include "/var/lib/samba/bind-dns/named.conf";" >> /etc/named.conf

#rm -f /var/lib/samba/private/secrets.ldb
#rm -f /var/lib/samba/private/secrets.tdb
