#!/bin/bash
#
# sambdadc.sh
#
# Autor     : Caio Bentes <caio.bentes@solustecnologia.com.br>
#
#  -------------------------------------------------------------
#   
#

#Atualiza a lista de pacotes e atualiza pacotes
apt-get update && apt-get upgrade -y
#Instalo dependencias
apt-get install -y vim acl attr autoconf bind9utils bison build-essential \
debhelper dnsutils docbook-xml docbook-xsl flex gdb libjansson-dev krb5-user \
libacl1-dev libaio-dev libarchive-dev libattr1-dev libblkid-dev libbsd-dev \
libcap-dev libcups2-dev libgnutls28-dev libgpgme-dev libjson-perl \
libldap2-dev libncurses5-dev libpam0g-dev libparse-yapp-perl \
libpopt-dev libreadline-dev nettle-dev perl perl-modules pkg-config \
python-all-dev python-crypto python-dbg python-dev python-dnspython \
python3-dnspython python-gpgme python3-gpgme python-markdown python3-markdown \
python3-dev xsltproc zlib1g-dev liblmdb-dev lmdb-utils ntp  isc-dhcp-server && echo OK


cp -pRf /etc/network/interfaces /etc/network/interfaces.bkp
echo "# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

auto enp0s3
  iface enp0s3 inet static
    address 192.168.10.142
    netmask 255.255.255.0
    gateway 192.168.10.1
    domain-name-servers 192.168.10.175" > /etc/network/interfaces

/etc/init.d/networking restart

echo "nameserver 192.168.10.175
search sindifisco.local" > /etc/resolv.conf
/etc/init.d/networking restart

#Seto as confirações do ip fixo do DC Samba
#read -p "Qual seu dominio? " MEUDOMINIO
read -p "Qual seu IP? " MEUIP
read -p "Qual sya máscara de rede IP? " MINHAMASCARADEREDE
read -p "Qual seu endereço de rede? " MEUENDERECODEREDE
read -p "Qual seu gateway? " MEUGATEWAY	
read -p "Qual seu broadcast da rede? " MEUBROADCAST

export MEUDOMINIO
export MEUIP
export MINHAMASCARADEREDE
export MEUENDERECODEREDE
export MEUGATEWAY
export MEUBROADCAST

sed -i 's/^iface enp0s3/#iface enp0s3/' /etc/network/interfaces
echo "# Static IP address" >> /etc/network/interfaces
echo "iface enp0s3 inet static" >> /etc/network/interfaces
echo "address ${MEUIP}" >> /etc/network/interfaces
echo "netmask ${MINHAMASCARADEREDE}" >> /etc/network/interfaces
echo "network ${MEUENDERECODEREDE}" >> /etc/network/interfaces
echo "broadcast ${MEUBROADCAST}" >> /etc/network/interfaces
echo  "gateway ${MEUGATEWAY}" >> /etc/network/interfaces
/etc/init.d/networking restart
ifup enp0s3


cp -pRf /etc/network/interfaces /etc/network/interfaces.bkp
echo "# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

auto enp0s3
  iface enp0s3 inet static
    address 192.168.10.142
    netmask 255.255.255.0
    gateway 192.168.10.1
    domain-name-servers 192.168.10.175" > /etc/network/interfaces



echo '#Implementing Samba 4
ntpsigndsocket /usr/local/samba/var/lib/ntp_signd/
restrict default mssntp' >> /etc/ntp.conf






read -p "Qual seu dominio? " MEUDOMINIO
read -p "Qual seu IP? " MEUIP
read -p "Qual seu endereço de rede? " MEUENDERECODEREDE
read -p "Qual seu gateway? " MEUGATEWAY
read -p "Qual seu broadcast da rede? " MEUBROADCAST
echo "# Implementing Samba 4
ddns-updates on;
option domain-name "sindifisco.local";
option domain-name-servers ${MEUIP}, ${MEUIP};
option netbios-name-servers ${MEUIP};
option ntp-servers ${MEUIP};
default-lease-time 600;
max-lease-time 7200;
log-facility local7;
subnet ${MEUENDERECODEREDE} netmask ${MINHAMASCARADEREDE} {
range 192.168.10.250 192.168.10.253;
option routers ${MEUIP}; }" >> /etc/dhcp/dhcpd.conf


sed -i '17,18s/^/#/' /etc/default/isc-dhcp-server
echo INTERFACES="enp0s3" >> /etc/default/isc-dhcp-server

/etc/init.d/isc-dhcp-server start

#Seto hostname
hostnamectl set-hostname fs01
systemctl restart systemd-hostnamed
#Faço meu host resolver pro meu ip
echo "192.168.10.142 fs01.sindifisco.local fs01" >> /etc/hosts

unset MEUIP
unset MINHAMASCARADEREDE
unset MEUENDERECODEREDE
unset MEUGATEWAY
unset MEUBROADCAST

#Restarto o serviço de Rede
systemctl restart networking

#Muso o diretório para o /tmp
cd /tmp
#Faço download do samba em sua sersão 4.8.3
wget https://download.samba.org/pub/samba/stable/samba-4.8.0.tar.gz
#Descompacto os arquivos do samba
tar -zxvf samba-4.8.0.tar.gz
#Removo o Samba compactado
rm -f samba-4.8.0.tar.gz
#Entro na pasta do samba
cd samba-4.8.0
#Vamos gerar o Makefile e verificar se não há nenhuma dependência faltando e alguns ajustes extras
#./configure --enable-debug --enable-selftest
./configure --prefix /usr --enable-fhs --enable-cups --enable-debug --enable-selftest --sysconfdir=/etc --localstatedir=/var --with-privatedir=/var/lib/samba/private --with-piddir=/var/run/samba --with-automount --datadir=/usr/share --with-lockdir=/var/run/samba --with-statedir=/var/lib/samba --with-cachedir=/var/cache/samba --with-systemd
#Vamos compilar o Samba 4
make
#Vamos a instalação dos arquivos, comandos, e bibliotecas nos seus respectivos diretórios
make quicktest
make install










#Restarto o serviço de Rede
/etc/init.d/network restart

#Seto hostname
hostnamectl set-hostname THOTH2
systemctl restart systemd-hostnamed
#Faço meu host resolver pro meu ip
echo "192.168.10.101 THOTH2.ativacob.br hathor2" >> /etc/hosts
#Instala o wget
yum install -y wget
#Desabilita o firewall
systemctl stop firewalld
systemctl disable firewalld
#Desabilita o SELinux
sed -i '/SELINUX/ s/enforcing/disabled/g' /etc/selinux/config
setenforce 0
echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf

#Instala pacotes nescessários para compilação do samba
yum install -y perl gcc attr libacl-devel libblkid-devel gnutls-devel readline-devel \
python-devel gdb pkgconfig krb5-workstation zlib-devel setroubleshoot-server \
libaio-devel setroubleshoot-plugins policycoreutils-python libsemanage-python \
perl-ExtUtils-MakeMaker perl-Parse-Yapp perl-Test-Base popt-devel libxml2-devel \
libattr-devel keyutils-libs-devel cups-devel bind-utils libxslt docbook-style-xsl \
openldap-devel autoconf python-crypto pam-devel ntp wget vim

#Adiciono configurações samba no ntp 
cat <<EOF >> /etc/ntp.conf
# Configurações adicionais para o Samba 4
ntpsigndsocket /var/lib/samba/ntp_signd/
restrict default mssntp:
EOF
#Restarto o serviço do ntp
systemctl restart ntpd

#Muso o diretório para o /tmp
cd /tmp
#Faço download do samba em sua sersão 4.8.3
wget https://download.samba.org/pub/samba/stable/samba-4.8.3.tar.gz
#Descompacto os arquivos do samba
tar -zxf samba-4.8.3.tar.gz
#Removo o Samba compactado
rm -f samba-4.8.3.tar.gz
#Entro na pasta do samba
cd samba-4.8.3
#Vamos gerar o Makefile e verificar se não há nenhuma dependência faltando e alguns ajustes extras
./configure --prefix /usr --enable-fhs --enable-cups --sysconfdir=/etc \
--localstatedir=/var --with-privatedir=/var/lib/samba/private --with-piddir=/var/run/samba \
--with-automount --datadir=/usr/share --with-lockdir=/var/run/samba --with-statedir=/var/lib/samba \
--with-cachedir=/var/cache/samba --with-systemd
#Vamos compilar o Samba 4
make
#Vamos a instalação dos arquivos, comandos, e bibliotecas nos seus respectivos diretórios
make install

ldconfig

cat <<EOF > /etc/krb5.conf
[libdefaults]
    dns_lookup_realm = false
    dns_lookup_kdc = true
    default_realm = SINDIFISCO.LOCAL
EOF


#Vamos atualizar o cache de bibliotecas dinâmicas
ldconfig
kinit ADMINISTRADOR
klist

systemctl restart network
net ads join -U administrador




#rm -f /var/lib/samba/private/secrets.ldb
#rm -f /var/lib/samba/private/secrets.tdb
